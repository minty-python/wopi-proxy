# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import mock
from wopi_proxy.views import wopi


class Test_Redis:
    @mock.patch("wopi_proxy.views.wopi.Redis")
    def test__intialize_redis(self, mock_redis):
        wopi._intialize_redis(host="host", port="port", password="pass", db=0)
        mock_redis.assert_called_once_with(
            host="host", port="port", password="pass", db=0
        )

        wopi._set_redis(name="name", value="value", ex=3600)
        mock_redis().set.assert_called_once_with("name", "value", 3600)

        wopi._get_redis(name="name")
        mock_redis().get.assert_called_once_with("name")

        wopi._get_ttl(name="name")
        mock_redis().ttl.assert_called_once_with("name")

        wopi._remove_redis(name="name")
        mock_redis().delete.assert_called_once_with("name")
