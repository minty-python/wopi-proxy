# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import io
import json
import mock
import pytest
import requests
from pyramid import httpexceptions as exec
from wopi_proxy.views import wopi


class Test_As_a_User_I_Want_To_Edit_Document_With_WOPI:
    """Test document edit with WOPI"""

    @property
    def mock_request(self):
        request = mock.MagicMock()
        request.configure_mock(
            configuration={
                "redis": {
                    "session": {"host": "host", "port": "port", "db": 0}
                },
                "wopi-proxy": {
                    "api_key": "123",
                    "discovery_url": "discovery_url",
                },
            },
            host="wopi.test.xyz.nl",
            json_body={
                "document_uuid": "document_uuid",
                "api_key": "123",
                "context": "test.xyz.nl",
                "app": "PowerPoint",
                "action": "edit",
                "business_user": False,
                "user_uuid": "user_uuid",
                "user_display_name": "user_display_name",
                "owner_uuid": "owner_uuid",
                "file_info": {
                    "extension": "pptx",
                    "filename": "presentation.pptx",
                    "size": 63921,
                    "version": 2,
                    "file_url": "file_url",
                },
                "callback_settings": {
                    "save_url": "save_url",
                    "save_url_params": {
                        "authentication_token": "authentication_token",
                        "document_uuid": "document_uuid",
                        "editor": "wopi",
                    },
                    "close_url": "close_url",
                    "host_page_url": "host_page_url",
                },
            },
        )
        return request

    @mock.patch("wopi_proxy.views.wopi.logging")
    @mock.patch("wopi_proxy.views.wopi.datetime")
    @mock.patch("wopi_proxy.views.wopi.secrets")
    @mock.patch("wopi_proxy.views.wopi.requests")
    @mock.patch("wopi_proxy.views.wopi._set_redis")
    @mock.patch("wopi_proxy.views.wopi._get_redis")
    def test_edit_document(
        self,
        mock_get_redis,
        mock_set_redis,
        mock_requests,
        mock_secerts,
        mock_datetime,
        mock_logging,
    ):
        mock_get_redis.return_value = None
        mock_discovery_url_response = mock.MagicMock()
        mock_discovery_url_response.text = (
            "<wopi-discovery> \n"
            + '<app name="PowerPoint" favIconUrl="test_favIconUrl"> \n'
            + '<action name="edit" ext="pptx" urlsrc="test_urlsrc&amp;&lt;ui=UI_LLCC&amp;&gt;&lt;rs=DC_LLCC&amp;&gt;&lt;dchat=DISABLE_CHAT&amp;&gt;&lt;hid=HOST_SESSION_ID&amp;&gt;&lt;sc=SESSION_CONTEXT&amp;&gt;&lt;wopisrc=WOPI_SOURCE&amp;&gt;&lt;showpagestats=PERFSTATS&amp;&gt;&lt;IsLicensedUser=BUSINESS_USER&gt;"/> \n'
            + "</app> \n"
            "</wopi-discovery>"
        )

        mock_requests.request.return_value = mock_discovery_url_response
        mock_secerts.token_urlsafe.return_value = "123"

        mock_datetime_now = datetime.datetime(2020, 10, 23, 13, 40, 21, 829328)
        mock_datetime.datetime.now.return_value = mock_datetime_now
        mock_datetime_delta = datetime.timedelta(hours=10)
        mock_datetime.timedelta.return_value = mock_datetime_delta

        res = wopi.edit_document(request=self.mock_request)

        assert res["favIconUrl"] == "test_favIconUrl"
        assert (
            res["urlsrc"]
            == "test_urlsrc&dchat=1&wopisrc=https://wopi.test.xyz.nl/wopi/files/document_uuid&IsLicensedUser=0"
        )
        assert res["access_token"] == "123"
        assert res["access_token_ttl"] == int(
            (mock_datetime_now + mock_datetime_delta).timestamp() * 1000.0
        )

        access_token_data = json.dumps(
            {
                "context": "test.xyz.nl",
                "document_uuid": "document_uuid",
                "action": "edit",
                "user_uuid": "user_uuid",
                "user_display_name": "user_display_name",
                "save_url": "save_url",
                "save_url_params": {
                    "authentication_token": "authentication_token",
                    "document_uuid": "document_uuid",
                    "editor": "wopi",
                },
                "close_url": "close_url",
                "host_edit_url": "host_page_url",
            }
        )
        document_data = json.dumps(
            {
                "document_uuid": "document_uuid",
                "extension": "pptx",
                "size": 63921,
                "filename": "presentation.pptx",
                "version": "2",
                "file_url": "file_url",
                "owner_uuid": "owner_uuid",
            }
        )
        mock_set_redis.assert_has_calls(
            [
                mock.call(
                    name="wopi_service:discovery_xml",
                    value=mock_discovery_url_response.text,
                    ex=86400,
                ),
                mock.call(
                    name="wopi_service:document:document_uuid",
                    value=document_data,
                    ex=36000,
                ),
                mock.call(
                    name="wopi_service:123",
                    value=access_token_data,
                    ex=36000,
                ),
            ]
        )
        mock_logging.getLogger().info.assert_has_calls(
            [
                mock.call(
                    "Called edit_document on file with uuid 'document_uuid' with json_body {'document_uuid': 'document_uuid', 'api_key': '123', 'context': 'test.xyz.nl', 'app': 'PowerPoint', 'action': 'edit', 'business_user': False, 'user_uuid': 'user_uuid', 'user_display_name': 'user_display_name', 'owner_uuid': 'owner_uuid', 'file_info': {'extension': 'pptx', 'filename': 'presentation.pptx', 'size': 63921, 'version': 2, 'file_url': 'file_url'}, 'callback_settings': {'save_url': 'save_url', 'save_url_params': {'authentication_token': 'authentication_token', 'document_uuid': 'document_uuid', 'editor': 'wopi'}, 'close_url': 'close_url', 'host_page_url': 'host_page_url'}}"
                ),
                mock.call(
                    "edit_document success for file with uuid document_uuid"
                ),
            ]
        )

    @mock.patch("wopi_proxy.views.wopi.requests")
    @mock.patch("wopi_proxy.views.wopi._get_redis")
    def test_edit_document_with_errors(self, mock_get_redis, mock_requests):

        mock_get_redis.return_value = (
            "<wopi-discovery> \n"
            + '<app name="PowerPoint" favIconUrl="test_favIconUrl"> \n'
            + '<action name="edit" ext="pptx"/> \n'
            + "</app> \n"
            + "</wopi-discovery>"
        )
        with pytest.raises(exec.HTTPNotFound) as exception_info:
            wopi.edit_document(request=self.mock_request)
        assert exception_info.value.json == {
            "errors": [
                {
                    "title": "No urlsrc found in discovery_xml for app 'PowerPoint' with action 'edit'"
                }
            ]
        }

        # When the document can not be edited by ms online.
        # In below example, discovery_xml only supports viewing of document and not editing.
        mock_get_redis.return_value = (
            "<wopi-discovery> \n"
            + '<app name="PowerPoint" favIconUrl="test_favIconUrl"> \n'
            + '<action name="view" ext="pptx" urlsrc="test_urlsrc&amp;&lt;ui=UI_LLCC&amp;&gt;&lt;rs=DC_LLCC&amp;&gt;&lt;dchat=DISABLE_CHAT&amp;&gt;&lt;hid=HOST_SESSION_ID&amp;&gt;&lt;sc=SESSION_CONTEXT&amp;&gt;&lt;wopisrc=WOPI_SOURCE&amp;&gt;&lt;showpagestats=PERFSTATS&amp;&gt;&lt;IsLicensedUser=BUSINESS_USER&gt;"/> \n'
            + "</app> \n"
            "</wopi-discovery>"
        )
        with pytest.raises(exec.HTTPNotFound) as exception_info:
            wopi.edit_document(request=self.mock_request)
        assert exception_info.value.json == {
            "errors": [
                {
                    "title": "No urlsrc found in discovery_xml for app 'PowerPoint' with action 'edit'"
                }
            ]
        }

        mock_get_redis.return_value = (
            "<wopi-discovery> \n"
            + '<app name="PowerPoint"> \n'
            + "<action/> \n"
            + "</app> \n"
            + "</wopi-discovery>"
        )
        with pytest.raises(exec.HTTPNotFound) as exception_info:
            wopi.edit_document(request=self.mock_request)
        assert exception_info.value.json == {
            "errors": [
                {
                    "title": "No favIconUrl found in discovery_xml for app 'PowerPoint'"
                }
            ]
        }

        mock_get_redis.return_value = (
            "<wopi-discovery> \n"
            + '<app name="Excel"> \n'
            + "<action/> \n"
            + "</app> \n"
            + "</wopi-discovery>"
        )
        with pytest.raises(exec.HTTPNotFound) as exception_info:
            wopi.edit_document(request=self.mock_request)
        assert exception_info.value.json == {
            "errors": [{"title": "No app 'PowerPoint' found in discovery_xml"}]
        }
        mock_get_redis.return_value = None
        mock_requests.request.side_effect = Exception(
            "Exception caught while reading discovery_xml"
        )
        with pytest.raises(exec.HTTPConflict) as exception_info:
            wopi.edit_document(request=self.mock_request)
        assert exception_info.value.json == {
            "errors": [
                {"title": "Exception caught while reading discovery_xml"}
            ]
        }

        mock_request = self.mock_request
        mock_request.json_body["api_key"] = "xxx"
        with pytest.raises(exec.HTTPUnauthorized) as exception_info:
            wopi.edit_document(request=mock_request)
        assert exception_info.value.json == {
            "errors": [{"title": "Authentication failed. Invalid api key."}]
        }

        del mock_request.json_body["api_key"]
        with pytest.raises(exec.HTTPBadRequest) as exception_info:
            wopi.edit_document(request=mock_request)
        assert exception_info.value.json == {
            "errors": [
                {
                    "title": "1 validation error for EditRequest\napi_key\n  field required (type=value_error.missing)"
                }
            ]
        }


class Test_As_a_User_I_Want_To_Get_Document_Info_For_WOPI:
    """Test get_document_info for WOPI"""

    def setup(self):
        self.request = mock.MagicMock()
        self.request.configure_mock(
            matchdict={"file_id": "document_uuid"},
            params={"access_token": "123"},
            headers={},
            configuration={"wopi-proxy": {}},
        )
        self.document_data = {
            "document_uuid": "document_uuid",
            "extension": "pptx",
            "size": 63921,
            "filename": "presentation.pptx",
            "version": "2",
            "file_url": "file_url",
            "owner_uuid": "owner_uuid",
        }
        self.access_token_data = {
            "document_uuid": "document_uuid",
            "context": "test.xyz.nl",
            "action": "edit",
            "user_uuid": "user_uuid",
            "user_display_name": "user_display_name",
            "close_url": "close_url",
            "host_edit_url": "host_edit_url",
        }

    @mock.patch("wopi_proxy.views.wopi.logging")
    @mock.patch("wopi_proxy.views.wopi._get_redis")
    def test_get_document_info(self, mock_get_redis, mock_logging):
        mock_get_redis.side_effect = [
            json.dumps(self.access_token_data),
            json.dumps(self.document_data),
            None,
        ]

        file_info = wopi.check_file_info(self.request)

        assert file_info["BaseFileName"] == self.document_data["filename"]
        assert file_info["Size"] == self.document_data["size"]
        assert file_info["OwnerId"] == self.document_data["owner_uuid"]
        assert file_info["UserId"] == self.access_token_data["user_uuid"]
        assert (
            file_info["UserFriendlyName"]
            == self.access_token_data["user_display_name"]
        )
        assert file_info["PostMessageOrigin"] == "test.xyz.nl"
        assert file_info["ClosePostMessage"] is True
        assert file_info["CloseUrl"] == "close_url"

        mock_logging.getLogger().info.has_calls(
            [
                mock.call(
                    "check_file_info called on file with uuid 'document_uuid'"
                ),
                mock.call(
                    "check_file_info success for file with uuid 'document_uuid' with response {file_info}"
                ),
            ]
        )

    @mock.patch("wopi_proxy.views.wopi._get_redis")
    def test_get_document_info_for_locked_odt_file(self, mock_get_redis):
        self.document_data["extension"] = "odt"
        self.document_data["filename"] = "doc.odt"

        mock_get_redis.side_effect = [
            json.dumps(self.access_token_data),
            json.dumps(self.document_data),
            "current_lock",
        ]
        res = wopi.check_file_info(self.request)
        assert res["ReadOnly"] is True

    @mock.patch("wopi_proxy.views.wopi._get_document_data")
    @mock.patch("wopi_proxy.views.wopi._validate_access_token")
    def test_get_document_info_with_dev_mode(
        self, mock_validate_access_token, mock_get_document_data
    ):
        mock_validate_access_token.return_value = self.access_token_data
        mock_get_document_data.return_value = self.document_data

        self.request.configuration["wopi-proxy"] = {"dev_mode": "true"}
        wopi.check_file_info(self.request)

    @mock.patch("wopi_proxy.views.wopi._get_redis")
    def test_get_document_info_with_errors(self, mock_get_redis):
        self.access_token_data["document_uuid"] = "unknown uuid"

        mock_get_redis.return_value = json.dumps(self.access_token_data)
        with pytest.raises(exec.HTTPUnauthorized) as excinfo:
            wopi.check_file_info(self.request)
        assert excinfo.value.json == {
            "errors": [{"title": "Invalid access token"}]
        }

        mock_get_redis.return_value = None
        with pytest.raises(exec.HTTPUnauthorized) as excinfo:
            wopi.check_file_info(self.request)
        assert excinfo.value.json == {
            "errors": [{"title": "Invalid access token"}]
        }

        self.request.matchdict = {}
        with pytest.raises(exec.HTTPBadRequest) as excinfo:
            wopi.check_file_info(self.request)
        assert excinfo.value.json == {
            "errors": [{"title": "Missing parameter 'file_id'"}]
        }


class Test_As_a_User_I_Want_To_Lock_A_File:
    """Test Locking a file in WOPI"""

    def setup(self):
        self.request = mock.MagicMock()
        self.request.configure_mock(
            matchdict={"file_id": "document_uuid"},
            params={"access_token": "123"},
            headers={"x_wopi_override": "LOCK", "x_wopi_lock": "x_wopi_lock"},
        )
        self.document_data = {
            "context": "test.xyz.nl",
            "document_uuid": "document_uuid",
            "action": "edit",
            "extension": "pptx",
            "size": 63921,
            "filename": "presentation.pptx",
            "version": "3",
            "file_url": "file_url",
            "user_uuid": "user_uuid",
            "user_display_name": "user_display_name",
            "owner_uuid": "owner_uuid",
            "save_url": "save_url",
            "save_url_params": {
                "authentication_token": "authentication_token",
                "save_url": "save_url",
            },
        }
        self.access_token_data = {
            "context": "test.xyz.nl",
            "document_uuid": "document_uuid",
            "action": "edit",
            "user_uuid": "user_uuid",
            "user_display_name": "user_display_name",
            "save_url": "save_url",
            "save_url_params": {
                "authentication_token": "authentication_token",
                "save_url": "save_url",
            },
        }

    @mock.patch("wopi_proxy.views.wopi.logging")
    @mock.patch("wopi_proxy.views.wopi._get_document_data")
    @mock.patch("wopi_proxy.views.wopi._validate_access_token")
    @mock.patch("wopi_proxy.views.wopi._set_redis")
    @mock.patch("wopi_proxy.views.wopi._get_redis")
    def test_lock(
        self,
        mock_get_redis,
        mock_set_redis,
        mock_validate_access_token,
        mock_get_document_data,
        mock_logging,
    ):
        mock_get_redis.return_value = b""
        mock_validate_access_token.return_value = self.access_token_data
        mock_get_document_data.return_value = self.document_data

        response = wopi.lock_file(self.request)
        mock_set_redis.assert_called_once_with(
            name="wopi_service:document_uuid:lock",
            value="x_wopi_lock",
            ex=1800,
        )
        response.body == "success"
        mock_logging.getLogger().info.assert_has_calls(
            [
                mock.call("lock called on file with uuid 'document_uuid'"),
                mock.call("lock success for file with uuid 'document_uuid'"),
            ]
        )

    @mock.patch("wopi_proxy.views.wopi._get_document_data")
    @mock.patch("wopi_proxy.views.wopi._validate_access_token")
    @mock.patch("wopi_proxy.views.wopi._get_redis")
    def test_lock_with_errors(
        self,
        mock_get_redis,
        mock_validate_access_token,
        mock_get_document_data,
    ):
        # When file is already locked and there is lock mismatch
        mock_get_redis.return_value = b"current_lock"
        mock_validate_access_token.return_value = self.access_token_data
        mock_get_document_data.return_value = self.document_data

        with pytest.raises(exec.HTTPConflict) as excinfo:
            wopi.lock_file(self.request)

        assert excinfo.value.json == {"errors": [{"title": "Lock mismatch"}]}
        assert excinfo.value.headers.get("X-WOPI-Lock") == "current_lock"
        assert (
            excinfo.value.headers.get("X-WOPI-LockFailureReason")
            == "Lock mismatch"
        )

        # When current lock is longer than maximum_lock_length(wich is 1024 ASCII chars )
        invalid_lock = (
            "current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_"
            + "current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_"
            + "current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_"
            + "current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_"
            + "current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_"
            + "current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_"
            + "lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_"
            + "lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_"
            + "lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock_current_lock"
        )
        self.request.headers["x_wopi_lock"] = invalid_lock
        mock_get_redis.return_value = invalid_lock.encode("utf-8")

        with pytest.raises(exec.HTTPConflict) as excinfo:
            wopi.lock_file(self.request)

        assert excinfo.value.json == {
            "errors": [{"title": "Lock is longer than 'maximum lock length'"}]
        }
        assert excinfo.value.headers.get("X-WOPI-Lock") == ""
        assert (
            excinfo.value.headers.get("X-WOPI-LockFailureReason")
            == "Lock is longer than 'maximum lock length'"
        )

        # When local has Non-ASCII charecters in it
        invalid_lock = "भारत"
        self.request.headers["x_wopi_lock"] = invalid_lock
        mock_get_redis.return_value = invalid_lock.encode("utf-8")

        with pytest.raises(exec.HTTPConflict) as excinfo:
            wopi.lock_file(self.request)

        assert excinfo.value.json == {
            "errors": [{"title": "Invalid lock. Non-ASCII characters found"}]
        }
        assert excinfo.value.headers.get("X-WOPI-Lock") == ""
        assert (
            excinfo.value.headers.get("X-WOPI-LockFailureReason")
            == "Lock has Non-ASCII characters"
        )

        # When X-WOPI-LOCK is not provided in request.
        del self.request.headers["x_wopi_lock"]
        with pytest.raises(exec.HTTPBadRequest) as excinfo:
            wopi.lock_file(self.request)

        assert excinfo.value.json == {
            "errors": [{"title": "X-WOPI-Lock is not provided"}]
        }

        # When X-WOPI-Override is not valid.
        self.request.headers["x_wopi_override"] = "INVALID_OVERRIDE"
        with pytest.raises(exec.HTTPBadRequest) as excinfo:
            wopi.lock_file(self.request)

        assert excinfo.value.json == {
            "errors": [{"title": "Invalid X-WOPI-Override"}]
        }


class Test_As_a_User_I_Want_To_Get_Lock_For_A_File:
    """Test Locking a file in WOPI"""

    def setup(self):
        self.request = mock.MagicMock()
        self.request.configure_mock(
            matchdict={"file_id": "document_uuid"},
            params={"access_token": "123"},
            headers={
                "x_wopi_override": "GET_LOCK",
                "x_wopi_lock": "current_lock",
            },
        )
        self.document_data = {
            "context": "test.xyz.nl",
            "document_uuid": "document_uuid",
            "action": "edit",
            "extension": "pptx",
            "size": 63921,
            "filename": "presentation.pptx",
            "version": "3",
            "file_url": "file_url",
            "user_uuid": "user_uuid",
            "user_display_name": "user_display_name",
            "owner_uuid": "owner_uuid",
            "save_url": "save_url",
            "save_url_params": {
                "authentication_token": "authentication_token",
                "save_url": "save_url",
            },
        }
        self.access_token_data = {
            "context": "test.xyz.nl",
            "document_uuid": "document_uuid",
            "action": "edit",
            "user_uuid": "user_uuid",
            "user_display_name": "user_display_name",
            "save_url": "save_url",
            "save_url_params": {
                "authentication_token": "authentication_token",
                "save_url": "save_url",
            },
        }

    @mock.patch("wopi_proxy.views.wopi.logging")
    @mock.patch("wopi_proxy.views.wopi._get_document_data")
    @mock.patch("wopi_proxy.views.wopi._validate_access_token")
    @mock.patch("wopi_proxy.views.wopi._get_redis")
    def test_get_lock_lock(
        self,
        mock_get_redis,
        mock_validate_access_token,
        mock_get_document_data,
        mock_logging,
    ):
        mock_get_redis.return_value = b"current_lock"
        mock_validate_access_token.return_value = self.access_token_data
        mock_get_document_data.return_value = self.document_data

        response = wopi.lock_file(self.request)
        response.body == "success"
        response.headerlist == [("X-WOPI-Lock", "current_lock")]

        mock_logging.getLogger().info.assert_has_calls(
            [
                mock.call("get_lock called on file with uuid 'document_uuid'"),
                mock.call(
                    "get_lock success for file with uuid 'document_uuid'"
                ),
            ]
        )


class Test_As_a_User_I_Want_To_Refresh_Lock_For_A_File:
    """Test Refresh lock for a file in WOPI"""

    def setup(self):
        self.request = mock.MagicMock()
        self.request.configure_mock(
            matchdict={"file_id": "document_uuid"},
            params={"access_token": "123"},
            headers={
                "x_wopi_override": "REFRESH_LOCK",
                "x_wopi_lock": "current_lock",
            },
        )
        self.document_data = {
            "context": "test.xyz.nl",
            "document_uuid": "document_uuid",
            "action": "edit",
            "extension": "pptx",
            "size": 63921,
            "filename": "presentation.pptx",
            "version": "3",
            "file_url": "file_url",
            "user_uuid": "user_uuid",
            "user_display_name": "user_display_name",
            "owner_uuid": "owner_uuid",
            "save_url": "save_url",
            "save_url_params": {
                "authentication_token": "authentication_token",
                "save_url": "save_url",
            },
        }
        self.access_token_data = {
            "context": "test.xyz.nl",
            "document_uuid": "document_uuid",
            "action": "edit",
            "user_uuid": "user_uuid",
            "user_display_name": "user_display_name",
            "save_url": "save_url",
            "save_url_params": {
                "authentication_token": "authentication_token",
                "save_url": "save_url",
            },
        }

    @mock.patch("wopi_proxy.views.wopi.logging")
    @mock.patch("wopi_proxy.views.wopi._get_document_data")
    @mock.patch("wopi_proxy.views.wopi._validate_access_token")
    @mock.patch("wopi_proxy.views.wopi._set_redis")
    @mock.patch("wopi_proxy.views.wopi._get_redis")
    def test_refresh_lock(
        self,
        mock_get_redis,
        mock_set_redis,
        mock_validate_access_token,
        mock_get_document_data,
        mock_logging,
    ):
        mock_get_redis.return_value = b"current_lock"
        mock_validate_access_token.return_value = self.access_token_data

        response = wopi.lock_file(self.request)
        mock_set_redis.assert_called_once_with(
            name="wopi_service:document_uuid:lock",
            value="current_lock",
            ex=1800,
        )
        response.body == "success"
        mock_logging.getLogger().info.assert_has_calls(
            [
                mock.call(
                    "refresh_lock called on file with uuid 'document_uuid'"
                ),
                mock.call(
                    "refresh_lock success for file with uuid 'document_uuid'"
                ),
            ]
        )

    @mock.patch("wopi_proxy.views.wopi._get_document_data")
    @mock.patch("wopi_proxy.views.wopi._validate_access_token")
    @mock.patch("wopi_proxy.views.wopi._get_redis")
    def test_refresh_lock_with_errors(
        self,
        mock_get_redis,
        mock_validate_access_token,
        mock_get_document_data,
    ):
        # When file is already locked and there is lock mismatch
        mock_get_redis.return_value = b"current_lock"
        mock_validate_access_token.return_value = self.access_token_data
        self.request.headers["x_wopi_lock"] = "new_lock"
        with pytest.raises(exec.HTTPConflict) as excinfo:
            wopi.lock_file(self.request)

        assert excinfo.value.json == {"errors": [{"title": "Lock mismatch"}]}
        assert excinfo.value.headers.get("X-WOPI-Lock") == "current_lock"
        assert (
            excinfo.value.headers.get("X-WOPI-LockFailureReason")
            == "Lock mismatch"
        )

        # When X-WOPI-LOCK is not provided in request.
        del self.request.headers["x_wopi_lock"]
        with pytest.raises(exec.HTTPBadRequest) as excinfo:
            wopi.lock_file(self.request)

        assert excinfo.value.json == {
            "errors": [{"title": "X-WOPI-Lock is not provided"}]
        }


class Test_As_a_User_I_Want_To_Unlock_A_File:
    """Test Unlock a file in WOPI"""

    def setup(self):
        self.request = mock.MagicMock()
        self.request.configure_mock(
            matchdict={"file_id": "document_uuid"},
            params={"access_token": "123"},
            headers={
                "x_wopi_override": "UNLOCK",
                "x_wopi_lock": "current_lock",
            },
        )
        self.document_data = {
            "context": "test.xyz.nl",
            "document_uuid": "document_uuid",
            "action": "edit",
            "extension": "pptx",
            "size": 63921,
            "filename": "presentation.pptx",
            "version": "3",
            "file_url": "file_url",
            "user_uuid": "user_uuid",
            "user_display_name": "user_display_name",
            "owner_uuid": "owner_uuid",
            "save_url": "save_url",
            "save_url_params": {
                "authentication_token": "authentication_token",
                "save_url": "save_url",
            },
        }
        self.access_token_data = {
            "context": "test.xyz.nl",
            "action": "edit",
            "document_uuid": "document_uuid",
            "user_uuid": "user_uuid",
            "user_display_name": "user_display_name",
            "save_url": "save_url",
            "save_url_params": {
                "authentication_token": "authentication_token",
                "save_url": "save_url",
            },
        }

    @mock.patch("wopi_proxy.views.wopi.logging")
    @mock.patch("wopi_proxy.views.wopi._get_document_data")
    @mock.patch("wopi_proxy.views.wopi._validate_access_token")
    @mock.patch("wopi_proxy.views.wopi._remove_redis")
    @mock.patch("wopi_proxy.views.wopi._get_redis")
    def test_unlock(
        self,
        mock_get_redis,
        mock_remove_redis,
        mock_validate_access_token,
        mock_get_document_data,
        mock_logging,
    ):
        mock_get_redis.return_value = b"current_lock"
        mock_validate_access_token.return_value = self.access_token_data
        mock_get_document_data.return_value = self.document_data
        response = wopi.lock_file(self.request)
        mock_remove_redis.assert_called_once_with(
            name="wopi_service:document_uuid:lock"
        )
        response.body == "success"
        mock_logging.getLogger().info.assert_has_calls(
            [
                mock.call("unlock called on file with uuid 'document_uuid'"),
                mock.call("unlock success for file with uuid 'document_uuid'"),
            ]
        )

    @mock.patch("wopi_proxy.views.wopi._get_document_data")
    @mock.patch("wopi_proxy.views.wopi._validate_access_token")
    @mock.patch("wopi_proxy.views.wopi._get_redis")
    def test_unlock_with_errors(
        self,
        mock_get_redis,
        mock_validate_access_token,
        mock_get_document_data,
    ):
        # When file is already locked and there is lock mismatch
        mock_get_redis.return_value = b"current_lock"
        mock_validate_access_token.return_value = self.access_token_data
        mock_get_document_data.return_value = self.document_data
        self.request.headers["x_wopi_lock"] = "new_lock"
        with pytest.raises(exec.HTTPConflict) as excinfo:
            wopi.lock_file(self.request)

        assert excinfo.value.json == {"errors": [{"title": "Lock mismatch"}]}
        assert excinfo.value.headers.get("X-WOPI-Lock") == "current_lock"
        assert (
            excinfo.value.headers.get("X-WOPI-LockFailureReason")
            == "Lock mismatch"
        )

        # When X-WOPI-LOCK is not provided in request.
        del self.request.headers["x_wopi_lock"]
        with pytest.raises(exec.HTTPBadRequest) as excinfo:
            wopi.lock_file(self.request)

        assert excinfo.value.json == {
            "errors": [{"title": "X-WOPI-Lock is not provided"}]
        }


class Test_As_a_User_I_Want_To_UnlockAndLock_A_File:
    """Test Unlock a file in WOPI"""

    def setup(self):
        self.request = mock.MagicMock()
        self.request.configure_mock(
            matchdict={"file_id": "document_uuid"},
            params={"access_token": "123"},
            headers={
                "x_wopi_override": "LOCK",
                "x_wopi_oldlock": "current_lock",
                "x_wopi_lock": "new_lock",
            },
        )
        self.access_token_data = {
            "context": "test.xyz.nl",
            "document_uuid": "document_uuid",
            "action": "edit",
            "user_uuid": "user_uuid",
            "user_display_name": "user_display_name",
            "save_url": "save_url",
            "save_url_params": {
                "authentication_token": "authentication_token",
                "save_url": "save_url",
            },
        }
        self.document_data = {
            "document_uuid": "document_uuid",
            "extension": "pptx",
            "size": 63921,
            "filename": "presentation.pptx",
            "version": "3",
            "file_url": "file_url",
            "owner_uuid": "owner_uuid",
        }

    @mock.patch("wopi_proxy.views.wopi.logging")
    @mock.patch("wopi_proxy.views.wopi._get_document_data")
    @mock.patch("wopi_proxy.views.wopi._validate_access_token")
    @mock.patch("wopi_proxy.views.wopi._get_ttl")
    @mock.patch("wopi_proxy.views.wopi._set_redis")
    @mock.patch("wopi_proxy.views.wopi._get_redis")
    def test_unlock_and_lock(
        self,
        mock_get_redis,
        mock_set_redis,
        mock_get_ttl,
        mock_validate_access_token,
        mock_get_document_data,
        mock_logging,
    ):
        mock_get_redis.return_value = b"current_lock"
        mock_validate_access_token.return_value = self.access_token_data
        mock_get_document_data.return_value = self.document_data
        mock_get_ttl.return_value = 300
        response = wopi.lock_file(self.request)
        mock_set_redis.assert_called_once_with(
            name="wopi_service:document_uuid:lock",
            value="new_lock",
            ex=300,
        )
        response.body == "success"
        mock_logging.getLogger().info.assert_has_calls(
            [
                mock.call(
                    "unlock_and_relock called on file with uuid 'document_uuid'"
                ),
                mock.call(
                    "unlock_and_relock success for file with uuid 'document_uuid'"
                ),
            ]
        )

    @mock.patch("wopi_proxy.views.wopi._get_document_data")
    @mock.patch("wopi_proxy.views.wopi._validate_access_token")
    @mock.patch("wopi_proxy.views.wopi._get_redis")
    def test_unlock_and_lock_with_errors(
        self,
        mock_get_redis,
        mock_validate_access_token,
        mock_get_document_data,
    ):
        # When file is already locked and there is lock mismatch
        mock_get_redis.return_value = b"current_lock"
        mock_validate_access_token.return_value = self.access_token_data
        mock_get_document_data.return_value = self.document_data
        self.request.headers["x_wopi_oldlock"] = "new_lock"
        with pytest.raises(exec.HTTPConflict) as excinfo:
            wopi.lock_file(self.request)

        assert excinfo.value.json == {"errors": [{"title": "Lock mismatch"}]}
        assert excinfo.value.headers.get("X-WOPI-Lock") == "current_lock"
        assert (
            excinfo.value.headers.get("X-WOPI-LockFailureReason")
            == "Lock mismatch"
        )

        # When X-WOPI-LOCK is not provided in request.
        del self.request.headers["x_wopi_lock"]
        with pytest.raises(exec.HTTPBadRequest) as excinfo:
            wopi.lock_file(self.request)

        assert excinfo.value.json == {
            "errors": [{"title": "X-WOPI-Lock is not provided"}]
        }


class Test_As_a_User_I_Want_To_Put_File_In_WOPI:
    """Test Locking a file in WOPI"""

    def setup(self):
        self.request = mock.MagicMock()
        self.request.configure_mock(
            matchdict={"file_id": "document_uuid"},
            params={"access_token": 123},
            headers={"x_wopi_override": "PUT", "x_wopi_lock": "current_lock"},
            body_file=io.BytesIO(b"test"),
        )
        self.access_token_data = {
            "context": "test.xyz.nl",
            "document_uuid": "document_uuid",
            "action": "edit",
            "user_uuid": "user_uuid",
            "user_display_name": "user_display_name",
            "save_url": "save_url",
            "save_url_params": {
                "authentication_token": "authentication_token",
                "save_url": "save_url",
            },
        }
        self.document_data = {
            "document_uuid": "document_uuid",
            "extension": "pptx",
            "size": 63921,
            "filename": "presentation.pptx",
            "version": "2",
            "file_url": "file_url",
            "owner_uuid": "owner_uuid",
        }

    @mock.patch("wopi_proxy.views.wopi.logging")
    @mock.patch("wopi_proxy.views.wopi._set_redis")
    @mock.patch("wopi_proxy.views.wopi._get_ttl")
    @mock.patch("wopi_proxy.views.wopi.requests")
    @mock.patch("wopi_proxy.views.wopi._validate_current_lock")
    @mock.patch("wopi_proxy.views.wopi._compare_locks")
    @mock.patch("wopi_proxy.views.wopi._get_redis")
    @mock.patch("wopi_proxy.views.wopi._get_document_data")
    @mock.patch("wopi_proxy.views.wopi._validate_access_token")
    def test_put_file(
        self,
        mock_validate_access_token,
        mock_get_document_data,
        mock_get_redis,
        mock_compare_locks,
        mock_validate_current_lock,
        mock_requests,
        mock_get_ttl,
        mock_set_redis,
        mock_logging,
    ):

        mock_validate_access_token.return_value = self.access_token_data
        mock_get_document_data.return_value = self.document_data
        mock_get_redis.return_value = b"current_lock"
        mock_get_ttl.return_value = 120

        res = mock.MagicMock()
        res.status_code = 200
        res.json.return_value = {
            "new_version": "3",
            "new_file_url": "new_file_url",
        }
        mock_requests.post.return_value = res

        response = wopi.put_file(self.request)

        assert response.body == b"Success"
        mock_get_redis.assert_called_once_with(
            "wopi_service:document_uuid:lock"
        )
        self.document_data.update(
            {"size": 4, "version": "3", "file_url": "new_file_url"}
        )
        mock_set_redis.assert_called_once_with(
            "wopi_service:document:document_uuid",
            json.dumps(self.document_data),
            120,
        )
        mock_logging.getLogger().info.assert_has_calls(
            [
                mock.call("put_file called on file with uuid 'document_uuid'"),
                mock.call(
                    "put_file success for file with uuid 'document_uuid'"
                ),
            ]
        )

    @mock.patch("wopi_proxy.views.wopi.requests")
    @mock.patch("wopi_proxy.views.wopi._validate_current_lock")
    @mock.patch("wopi_proxy.views.wopi._compare_locks")
    @mock.patch("wopi_proxy.views.wopi._get_redis")
    @mock.patch("wopi_proxy.views.wopi._get_document_data")
    @mock.patch("wopi_proxy.views.wopi._validate_access_token")
    def test_put_file_with_errors(
        self,
        mock_validate_access_token,
        mock_get_document_data,
        mock_get_redis,
        mock_compare_locks,
        mock_validate_current_lock,
        mock_requests,
    ):
        mock_get_redis.return_value = b"current_lock"
        mock_validate_access_token.return_value = {
            "context": "test.xyz.nl",
            "document_uuid": "document_uuid",
            "action": "edit",
            "user_uuid": "user_uuid",
            "user_display_name": "user_display_name",
            "save_url": "save_url",
            "save_url_params": {
                "authentication_token": "authentication_token",
                "save_url": "save_url",
            },
        }
        mock_get_document_data.return_value = {
            "document_uuid": "document_uuid",
            "extension": "pptx",
            "size": 63921,
            "filename": "presentation.pptx",
            "version": "2",
            "file_url": "file_url",
            "owner_uuid": "owner_uuid",
        }

        # When WOPI client throws error while trying to save document using 'save_url'
        res = mock.MagicMock()
        res.status_code = 409
        res.raise_for_status.side_effect = exec.HTTPConflict(
            "Can not create file"
        )
        mock_requests.post.return_value = res
        with pytest.raises(exec.HTTPConflict):
            wopi.put_file(self.request)

        # When File is unlocked but is not empty (has size more than 0 bytes)
        mock_get_redis.return_value = None
        with pytest.raises(exec.HTTPConflict) as exc_info:
            wopi.put_file(self.request)

        assert exc_info.value.json == {
            "errors": [
                {
                    "title": "Unlocked file with UUID 'document_uuid' is not empty."
                }
            ]
        }

        # When X-WOPI-Override header value is "PUT_RELATIVE"
        self.request.headers["x_wopi_override"] = "PUT_RELATIVE"
        with pytest.raises(exec.HTTPNotImplemented) as exc_info:
            wopi.put_file(self.request)
        assert exc_info.value.json == {
            "errors": [{"title": "PutRelativeFile is not Implemeneted"}]
        }

        # When X-WOPI-Override header value is not "PUT"
        self.request.headers["x_wopi_override"] = "UNLOCK"
        with pytest.raises(exec.HTTPBadRequest) as exc_info:
            wopi.put_file(self.request)
        assert exc_info.value.json == {
            "errors": [{"title": "Invalid X-WOPI-Override"}]
        }


class Test_As_a_User_I_Want_To_Get_File_In_WOPI:
    """Test Get file in WOPI"""

    def setup(self):
        self.request = mock.MagicMock()
        self.request.configure_mock(
            matchdict={"file_id": "document_uuid"},
            headers={},
        )

    @mock.patch("wopi_proxy.views.wopi._get_document_data")
    @mock.patch("wopi_proxy.views.wopi.logging")
    @mock.patch("wopi_proxy.views.wopi.requests")
    @mock.patch("wopi_proxy.views.wopi._validate_access_token")
    def test_get_file(
        self,
        mock_validate_access_token,
        mock_requests,
        mock_logging,
        mock_get_document_data,
    ):
        mock_validate_access_token.return_value = {
            "context": "test.xyz.nl",
            "document_uuid": "document_uuid",
            "user_uuid": "user_uuid",
            "user_display_name": "user_display_name",
            "save_url": "save_url",
            "save_url_params": {
                "authentication_token": "authentication_token",
                "save_url": "save_url",
            },
        }
        mock_get_document_data.return_value = {
            "document_uuid": "document_uuid",
            "extension": "pptx",
            "size": 63921,
            "filename": "presentation.pptx",
            "version": "2",
            "file_url": "file_url",
            "owner_uuid": "owner_uuid",
        }
        response = mock.MagicMock()
        response.configure_mock(status=200, content=b"binary_content")
        mock_requests.get.return_value = response

        result = wopi.get_file(self.request)
        mock_requests.get.assert_called_once_with(url="file_url", verify=False)
        assert result.body == response.content
        mock_logging.getLogger().info.assert_has_calls(
            [
                mock.call("get_file called on file with uuid 'document_uuid'"),
                mock.call(
                    "get_file success for file with uuid 'document_uuid'"
                ),
            ]
        )

    @mock.patch("wopi_proxy.views.wopi._get_document_data")
    @mock.patch("wopi_proxy.views.wopi.requests")
    @mock.patch("wopi_proxy.views.wopi._validate_access_token")
    def test_get_file_with_error(
        self, mock_validate_access_token, mock_requests, mock_get_document_data
    ):
        mock_validate_access_token.return_value = {
            "context": "test.xyz.nl",
            "document_uuid": "document_uuid",
            "action": "edit",
            "user_uuid": "user_uuid",
            "user_display_name": "user_display_name",
            "save_url": "save_url",
            "save_url_params": {
                "authentication_token": "authentication_token",
                "save_url": "save_url",
            },
        }
        mock_get_document_data.return_value = {
            "document_uuid": "document_uuid",
            "extension": "pptx",
            "size": 63921,
            "filename": "presentation.pptx",
            "version": "2",
            "file_url": "file_url",
            "owner_uuid": "owner_uuid",
        }
        response = mock.MagicMock()

        def raise_for_status():
            raise requests.HTTPError()

        response.configure_mock(
            status=500,
            content=b"Server error",
            raise_for_status=raise_for_status,
        )
        mock_requests.get.return_value = response
        with pytest.raises(requests.HTTPError):
            wopi.get_file(self.request)


class Test_health_check_for_WOPI:
    def setup(self):
        self.request = mock.MagicMock()
        self.request.configure_mock(
            configuration={
                "redis": {"session": {"host": "host", "port": "port"}},
                "wopi-proxy": {
                    "api_key": "123",
                    "discovery_url": "https://ffc-onenote.officeapps.live.com/hosting/discovery",
                },
                "statsd": {"host": "host", "port": "port"},
            },
        )

    def test_health_check(self):
        res = wopi.health_check(self.request)
        assert res.status == "200 OK"
        assert res.body == b"Success"

    def test_health_check_failed(self):
        self.request.configuration["wopi-proxy"][
            "discovery_url"
        ] = "http//invalid url"
        with pytest.raises(exec.HTTPInternalServerError) as excinfo:
            wopi.health_check(self.request)

        assert excinfo.value.json == {
            "errors": [
                {
                    "title": "There was a conflict when trying to complete your request."
                }
            ]
        }

        del self.request.configuration["redis"]["session"]["host"]
        with pytest.raises(exec.HTTPInternalServerError) as excinfo:
            wopi.health_check(self.request)

        assert excinfo.value.json == {
            "errors": [
                {
                    "title": "_intialize_redis() missing 1 required positional "
                    "argument: 'host'"
                }
            ]
        }

        del self.request.configuration["redis"]["session"]
        with pytest.raises(exec.HTTPInternalServerError) as excinfo:
            wopi.health_check(self.request)
        assert excinfo.value.json == {
            "errors": [{"title": "Missing parameter 'session'"}]
        }
