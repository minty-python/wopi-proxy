# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

# AUTOGENERATED FILE - No need to edit
# GENERATED AT: 2020-11-12T20:32:57"
# Re-create by running 'generate-routes' command
# Run 'generate-routes --help' for more options


def add_routes(config):
    """Add routes to pyramid application.

    :param config: pyramid config file
    :type config: Configurator
    """
    handlers = [
        {
            "route": "/api/documentopenapi/{filename}",
            "handler": "apidocs_fileserver",
            "method": "GET",
            "view": "wopi_proxy.views.apidocs.apidocs_fileserver",
        },
        {
            "route": "/api/document/edit",
            "handler": "edit_document",
            "method": "POST",
            "view": "wopi_proxy.views.wopi.edit_document",
        },
        {
            "route": "/wopi/files/{file_id}",
            "handler": "check_file_info",
            "method": "GET",
            "view": "wopi_proxy.views.wopi.check_file_info",
        },
        {
            "route": "/wopi/files/{file_id}",
            "handler": "lock_file",
            "method": "POST",
            "view": "wopi_proxy.views.wopi.lock_file",
        },
        {
            "route": "/wopi/files/{file_id}/contents",
            "handler": "put_file",
            "method": "POST",
            "view": "wopi_proxy.views.wopi.put_file",
        },
        {
            "route": "/wopi/files/{file_id}/contents",
            "handler": "get_file",
            "method": "GET",
            "view": "wopi_proxy.views.wopi.get_file",
        },
        {
            "route": "/api/health_check",
            "handler": "health_check",
            "method": "GET",
            "view": "wopi_proxy.views.wopi.health_check",
        },
    ]

    for h in handlers:
        config.add_route(
            name=h["handler"], pattern=h["route"], request_method=h["method"]
        )
        config.add_view(
            view=h["view"],
            route_name=h["handler"],
            request_method=h["method"],
        )
