# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import enum
from pydantic import BaseModel, Field
from typing import Optional


class AppEnum(str, enum.Enum):
    excel = "Excel"
    word = "Word"
    onenote = "OneNote"
    powerpoint = "PowerPoint"
    visio = "Visio"
    wopitest = "WopiTest"
    wordpdf = "WordPdf"
    wordprague = "WordPrague"


class FileInfo(BaseModel):
    extension: str = Field(..., description="The extension of the file")
    filename: str = Field(..., description="Full name of file")
    version: str = Field(
        ..., description="The current version of file as string"
    )
    file_url: str = Field(
        ..., description="Temporary s3 url for MS to retrieve the file from"
    )
    size: int = Field(..., description="Size of file in bytes")


class CallBackSettings(BaseModel):
    save_url: str = Field(
        ..., description="Url provided by client to save document in client."
    )
    save_url_params: Optional[dict] = Field(
        {}, description="Params for save_url"
    )
    close_url: str = Field(
        ...,
        description="A URI to a web page that the WOPI client should navigate to when the application closes",
    )
    host_page_url: str = Field(
        ..., description="Url of Host page where the wopi action loads"
    )


class EditRequest(BaseModel):
    api_key: str = Field(
        ...,
        description="A configured key used to authenticate requests",
    )
    app: AppEnum = Field(..., description="The type the file application")
    context: str = Field(
        ..., description="The hostname of the instance to retrieve the file"
    )
    document_uuid: str = Field(..., description="UUID of document/file")
    action: str = Field(..., description="The action to run for this file")
    file_info: FileInfo = Field(..., description="Information about file.")
    user_uuid: str = Field(
        ..., description="UUID of the use currently accessing the file"
    )
    user_display_name: str = Field(..., description="Display name of user")
    owner_uuid: str = Field(..., description="UUID of the owner of the file")
    business_user: Optional[bool] = Field(
        False, description="Boolean to check if user is business user"
    )
    callback_settings: CallBackSettings = Field(
        ...,
        description="Setting for call back WOPI client.",
    )
